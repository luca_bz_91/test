package com.example.luca.testapp;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Luca on 15/04/2015.
 */
public class APIRequest {

    private Context context;
    private JSONArray results;
    private List<Program> programs = new ArrayList<Program>();

    public APIRequest(Context context){
        this.context = context;
    }

    public void getProgramsFromURL(String url){
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>(){

                    @Override
                    public void onResponse(String response) {
                        try {
                            results = new JSONArray(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Parsing failed", Toast.LENGTH_LONG).show();
                        }
                        try {
                            for(int i = 0; i < results.length(); i++) {
                                JSONObject jsonObject = results.getJSONObject(i);
                                Program program = new Program(jsonObject.getString("name"), jsonObject.getString("description"));
                                programs.add(program);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Server Error", Toast.LENGTH_LONG).show();
            }
        });

        requestQueue.add(stringRequest);

    }

    public List<Program> getPrograms(){
        return programs;
    }

}
