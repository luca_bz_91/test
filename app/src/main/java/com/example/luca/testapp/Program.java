package com.example.luca.testapp;

/**
 * Created by Luca on 15/04/2015.
 */
public class Program {

    String name;
    String description;

    public Program(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
