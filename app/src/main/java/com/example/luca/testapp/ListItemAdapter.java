package com.example.luca.testapp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by Luca on 15/04/2015.
 */
public class ListItemAdapter extends ArrayAdapter<Program> {

    Context context;
    List<Program> programs;

    public ListItemAdapter(Context context, int resource, List<Program> programs){
        super(context, resource, programs);
        this.context = context;
        this.programs = programs;
        Toast.makeText(this.context, "Programs adapter: " + programs.size(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getCount() {
        return programs.size();
    }

    @Override
    public Program getItem(int position) {
        return programs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            convertView = LayoutInflater.from(context).inflate(R.layout.program_item, parent);
            convertView = inflater.inflate(R.layout.program_item, parent, false);
        }
        Program p = getItem(position);
        TextView name = (TextView) convertView.findViewById(R.id.progName);
        TextView description = (TextView) convertView.findViewById((R.id.progDescr));
        name.setText(p.getName());
        description.setText(p.getDescription().substring(0,50) + "... (Click to see more)");
        name.setTextColor(Color.BLACK);
        description.setTextColor(Color.BLACK);
        return convertView;
    }
}
